"""surveillance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from surveillanceApps import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', include('surveillanceApps.urls'),name='index'),
    url(r'^index/ajax', views.alerts, name='ajax'),
    url(r'^index/loginUser', views.loginUser, name='loginUser'),
    url(r'^index/runapps', views.runApps, name='runapps'),
    url(r'^index/action', views.action, name='action'),
    url(r'^login/$', auth_views.login, {'template_name': 'app/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout,{'template_name': 'app/login.html'}, name='logout'),
    url(r'^index/dashboard', views.analyzeResults, name='analyzeAlerts'),
    url(r'^index/history', views.alertHistory, name='pastAlerts'),    
    url(r'^index/update', views.updateCamera, name='updateCamera'),  
    url(r'^index/report', views.genReport, name='generateReport'),  

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
