import cv2

def contains_human_contour(img, model, lr=0.05, numIter=6, shadowSet=True, filterSize=(21,21), areaThresh=0.01):
    flag = True
    if img is None:
        return False
    totalSize = img.size
    img = cv2.GaussianBlur(img, filterSize, 0)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    fgmask = model.apply(img,learningRate=lr)
    if shadowSet==True:
        fgmask[fgmask==127] = 255
    else:
        fgmask[fgmask==127] = 0

    thresh = cv2.dilate(fgmask, None, iterations=numIter)
    _,cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    if len(cnts) > 0:
        humanCnt = sorted(cnts, key = cv2.contourArea, reverse = True)[0]
        # cv2.drawContours(img, [humanCnt], -1, (0, 255, 0), 3)
        fractionOfArea = (cv2.contourArea(humanCnt))/totalSize
        if fractionOfArea < areaThresh:
            flag = False
        #print("Area", fractionOfArea)
    else:
        print("No contour found")
        flag = False
        
    return flag
