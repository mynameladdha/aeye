from django.shortcuts import render,redirect
import cv2
import requests
from django.http import HttpResponse, StreamingHttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import numpy as np
import time
from .forms import CameraForm
from .models import Camera, Alerts, Application
import threading
import multiprocessing
from . import YOLO_tiny_tf
from .utils import non_max_suppression_fast, is_now_in_time_period
from .foreground_background import contains_human_contour
from uuid import uuid4
import datetime
import json
import imutils
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .camera import VideoCamera
from .intrusion import IntrusionDetection
#from .facial import FaceDetection
from .tampering import TamperingDetection
from .perimeter import PerimeterMonitoring
from .constants import USERNAME,PASSWORD,ACTION_TO_ALERT,PERIMETER,INTRUSION,TAMPERING,OVERCROWDING,STARTX_REC,STARTY_REC,WIDTH_REC,HEIGHT_REC,CAMERA_NAME,CAMERA_URL,CLIP_PATH,IMAGE_URL,UPDATE,DELETE,UPDATEDNAME,CURRENTNAME
from django.core.urlresolvers import reverse
from django.db.models import Count
from .allApps import App,thread_dic#,yolo
from reportlab.pdfgen import canvas
from reportlab.lib.colors import PCMYKColor
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart
from reportlab.graphics.charts.piecharts import Pie
from reportlab.graphics import renderPDF



@login_required()
def genReport(request):
    listOfCounts=[0]*4
    relevantIrrelevant=[0]*3
    allAlerts=Alerts.objects.all().values('alertType').annotate(total=Count('alertType'))
    relIrrelevant=Alerts.objects.all().values('relevance').annotate(total=Count('relevance'))
    for i in allAlerts:
        if i['alertType']==INTRUSION:
            listOfCounts[0]=i['total']
        elif i['alertType']==PERIMETER:
            listOfCounts[1]=i['total']
        elif i['alertType']==OVERCROWDING:
            listOfCounts[2]=i['total']
        elif i['alertType']==TAMPERING:
            listOfCounts[3]=i['total']
    for i in relIrrelevant:
        if i['relevance']=="Relevant":
            relevantIrrelevant[0]=i['total']
        elif i['relevance']=="Irrelevant":
            relevantIrrelevant[1]=i['total']
        elif i['relevance']=="not yet updated":
            relevantIrrelevant[2]=i['total']
    response=HttpResponse(content_type='application/pdf')
    response['Content-Disposition']='attachement;filename="report.pdf"'
    p=canvas.Canvas(response)
    drawing=Drawing(900,150)
    bar=VerticalBarChart()
    bar.x=100
    bar.y=100
    bar.barWidth=50
    bar.barSpacing=20
    data=[listOfCounts]
    bar.data=data
    bar.categoryAxis.categoryNames=[INTRUSION,PERIMETER,OVERCROWDING,TAMPERING]
    bar.categoryAxis.labels.fontSize=5
    bar.bars[0].fillColor=PCMYKColor(0,100,100,40,alpha=85)
    bar.bars[1].fillColor=PCMYKColor(23,51,0,4,alpha=85)
    bar.bars.fillColor=PCMYKColor(100,0,90,50,alpha=85)
    drawing.add(bar,'')
    p.drawString(0,800,"Analysis")
    p.drawString(0,760,"Type of alert and its frequency:")
    renderPDF.draw(drawing,p,0,560)
    p.drawString(0,569,"Distribution of alerts according to relevance:")
    drawing=Drawing()
    pie=Pie()
    pie.x=100
    pie.y=100
    pie.data=relevantIrrelevant
    pie.labels=["relevant","irrelevant","not yet updated"]
    pie.slices.strokeWidth=0.5
    drawing.add(pie)
    renderPDF.draw(drawing,p,0,340)
    p.showPage()
    p.save()
    return response


@login_required()
def analyzeResults(request):
    listOfCounts=[0]*4
    relevantIrrelevant=[0]*3
    allAlerts=Alerts.objects.all().values('alertType').annotate(total=Count('alertType'))
    relIrrelevant=Alerts.objects.all().values('relevance').annotate(total=Count('relevance'))
    for i in allAlerts:
        if i['alertType']==INTRUSION:
            listOfCounts[0]=i['total']
        elif i['alertType']==PERIMETER:
            listOfCounts[1]=i['total']
        elif i['alertType']==OVERCROWDING:
            listOfCounts[2]=i['total']
        elif i['alertType']==TAMPERING:
            listOfCounts[3]=i['total']
    for i in relIrrelevant:
        if i['relevance']=="Relevant":
            relevantIrrelevant[0]=i['total']
        elif i['relevance']=="Irrelevant":
            relevantIrrelevant[1]=i['total']
        elif i['relevance']=="not yet updated":
            relevantIrrelevant[2]=i['total']
        


    return render(request, 'app/dashboard.html', {'list':json.dumps(listOfCounts),'list1':json.dumps(relevantIrrelevant)})

@login_required()
def alertHistory(request):
    allAlerts=Alerts.objects.order_by('time').reverse()
    paginator = Paginator(allAlerts, 5)
    page = request.GET.get('page')
    try:
        alerts = paginator.page(page)
    except PageNotAnInteger:
        alerts = paginator.page(1)
    except EmptyPage:
        alerts = paginator.page(paginator.num_pages)
    return render(request, 'app/history.html', {'alerts':alerts})





@login_required()
def alerts(request):
    response_dict = {}
    if request.is_ajax():
        latestPendingAlert = Alerts.objects.filter(
            checked=False).order_by('-time')[:1]
        response_dict = {'alerts': list(latestPendingAlert.values())}

    return HttpResponse(json.dumps(response_dict))


def loginUser(request):
    print("mayank")
    if request.method == "POST":

        username = request.POST[USERNAME]
        password = request.POST[PASSWORD]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            form = CameraForm()
            allCameras = Camera.objects.all()
            return redirect(home)

    return redirect('login')



@login_required()
def updateCamera(request):
    form = CameraForm()
    allCameras = Camera.objects.all()

    if request.method == "POST":
        camobj=Camera.objects.get(url=request.POST[IMAGE_URL])
        if UPDATE in request.POST:
            updName=request.POST[UPDATEDNAME]
            camobj.camName=updName
            camobj.save()
        if DELETE in request.POST:
            camobj.delete()
               
        
    return redirect(home)


# Create your views here.
@login_required()
def home(request):
    form = CameraForm()
    allCameras = Camera.objects.all()
    
    if request.method == "POST":
        form = CameraForm(request.POST)
        if form.is_valid():
            cameraName = form.cleaned_data[CAMERA_NAME]
            cameraUrl = form.cleaned_data[CAMERA_URL]
            cameraUsername = form.cleaned_data[USERNAME]
            cameraPassword = form.cleaned_data[PASSWORD]

            # print(cameraUrl)
            new = Camera.objects.create(camName=cameraName, url=cameraUrl,
                                        username=cameraUsername, password=cameraPassword, uid=request.user)
            allCameras = Camera.objects.all()
    return render(request, 'app/index.html', {'form': form, 'allcam': allCameras})


@login_required()
def action(request):
    form = CameraForm()
    allCameras = Camera.objects.all()
    if request.method == "POST":
        if ACTION_TO_ALERT in request.POST:
            print("mayank")
            relValue = request.POST[ACTION_TO_ALERT]
            clipPath = request.POST[CLIP_PATH]
            print(clipPath)
            alertChecked = Alerts.objects.get(clip=clipPath)
            alertChecked.checked = True
            alertChecked.relevance = relValue
            currTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            alertChecked.responseTime = currTime
            alertChecked.save()

    return render(request, 'app/index.html', {'form': form, 'allcam': allCameras})


def logout_view(request):
    logout(request)


@login_required()
def runApps(request):
    form = CameraForm()
    allCameras = Camera.objects.all()
    if request.method == "POST":
        print("laddha")
        url = request.POST[IMAGE_URL]
        print("mayank " + url)
        if INTRUSION in request.POST or PERIMETER in request.POST or TAMPERING in request.POST or OVERCROWDING in request.POST:

            camobj = Camera.objects.get(url=url)

            if INTRUSION in request.POST:
                #print("intrusion started")
                stTime=""
                endTime=""
                if "intrusion_sttime" in  request.POST:
                    stTime=request.POST['intrusion_sttime']
                if "intrusion_endtime" in  request.POST:
                    endTime=request.POST['intrusion_endtime']    
                newIntrusionObj=IntrusionDetection(camurl=url, camuname=camobj.username, campassword=camobj.password, req=request,sttime=stTime,endtime=endTime)
                curr=time.time()
                t = threading.Thread(target=newIntrusionObj.intrusion_detection, args=(), kwargs={},name=url+"Intrusion"+" "+str(curr))
                t.setDaemon(True)
                t.start()
                thread_dic[url+"Intrusion"]=str(curr)
            if TAMPERING in request.POST:
                curr=time.time()
                newTamperingObj= TamperingDetection(camurl=url, camuname=camobj.username, campassword=camobj.password, req=request)
                t = threading.Thread(target=newTamperingObj.camera_tampering, args=(), kwargs={},name=url+"Tampering"+" "+str(curr))
                t.setDaemon(True)
                t.start()
                thread_dic[url+"Tampering"]=str(curr)
            if PERIMETER in request.POST:
                curr=time.time()
                if STARTX_REC in request.POST:
                    startx = request.POST[STARTX_REC]
                    print(startx)
                if STARTY_REC in request.POST:
                    starty=request.POST[STARTY_REC]
                if WIDTH_REC in request.POST:
                    width=request.POST[WIDTH_REC]
                if HEIGHT_REC in request.POST:
                    height=request.POST[HEIGHT_REC]
                stTime=""
                endTime=""
                if "perimeter_sttime" in  request.POST:
                    stTime=request.POST['perimeter_sttime']
                if "perimeter_endtime" in  request.POST:
                    endTime=request.POST['perimeter_endtime']      
                newPerimeterObj= PerimeterMonitoring(camurl=url,camuname=camobj.username,campassword=camobj.password,req=request,startx=startx,starty=starty,width=width,height=height,sttime=stTime,endtime=endTime)
                t = threading.Thread(target=newPerimeterObj.perimeter_monitoring, args=(), kwargs={},name=url+"Perimeter"+" "+str(curr))
                t.setDaemon(True)
                t.start()
                thread_dic[url+"Perimeter"]=str(curr)
            
                

    return render(request, 'app/index.html', {'form': form, 'allcam': allCameras})
