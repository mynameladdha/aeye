import cv2
import numpy as np
from .models import Camera, Alerts, Application
import threading
from . import YOLO_tiny_tf
from .utils import non_max_suppression_fast, is_now_in_time_period
from .foreground_background import contains_human_contour
from uuid import uuid4
import datetime
import json
import imutils
import time
from .camera import VideoCamera
from abc import ABC, abstractmethod
from .allApps import App,thread_dic#yolo,
from .constants import USERNAME,PASSWORD,ACTION_TO_ALERT,PERIMETER,INTRUSION,TAMPERING,OVERCROWDING,STARTX_REC,STARTY_REC,WIDTH_REC,HEIGHT_REC,CAMERA_NAME,CAMERA_URL,CLIP_PATH,IMAGE_URL,TAMPERING_MEDIA

    	
class TamperingDetection(App):

    def __init__(self,camurl, camuname, campassword, req):
        self.camurl=camurl
        self.camuname=camuname
        self.campassword=campassword
        self.req=req
        self.cam=None
        self.camobj=None


    def initialize_camera(self):
        self.cam = VideoCamera(self.camurl, self.camuname, self.campassword)

    def add_application(self):
        self.camobj = Camera.objects.get(url=self.camurl)
        Application.objects.create(cid=self.camobj, appName=TAMPERING, uid=self.req.user)

    def camera_tampering(self):
        print("started")
        l=threading.current_thread().getName().split(" ")
        checkToStoptime=l[1]
        checkToStopurl=l[0]
        self.initialize_camera()
        self.add_application()
        baseImg=cv2.imread(TAMPERING_MEDIA+str(self.camobj.camName)+'.png')
        baseImg_resized=cv2.resize(baseImg,(640,480))
        #print("baseImg: ",baseImg_resized.shape)
        grayA = cv2.cvtColor(baseImg_resized, cv2.COLOR_BGR2GRAY)
        histBaseImg=cv2.calcHist([grayA], [0], None, [256], [0, 256])
        histBaseImg_normalized=cv2.normalize(histBaseImg,histBaseImg).flatten()

        while True:
            if checkToStopurl in thread_dic and checkToStoptime<thread_dic[checkToStopurl]:
                print("stopping previous thread")
                break
            img = self.cam.get_frame()
            if img is None:
                continue
            #print(img.shape)
            #print(threading.current_thread().ident,datetime.datetime.now())
            grayB = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            #(score, diff) = compare_ssim(grayA, grayB, full=True)
            #print(score)
            histCurrFrame = cv2.calcHist([grayB], [0], None, [256], [0, 256])
            #print(histCurrFrame)
            histCurrFrame_normalized=cv2.normalize(histCurrFrame,histCurrFrame).flatten()
            isblocked=False
            diff=cv2.compareHist(histBaseImg_normalized, histCurrFrame_normalized,cv2.HISTCMP_CORREL)
            #print(diff)
            for i in histCurrFrame:
                if i[0]> (baseImg_resized.shape[0]*baseImg_resized.shape[1])/2:
                    isblocked=True

            if isblocked and diff<0.4:
                self.filename = uuid4().hex
                cv2.imwrite(TAMPERING_MEDIA + self.filename + ".jpg", img)
                print("tampering completed")
                self.add_alert()
                time.sleep(20)

            elif diff<0.3:
                self.filename = uuid4().hex
                cv2.imwrite(TAMPERING_MEDIA + self.filename + ".jpg", img)
                print("tampering completed")
                self.add_alert()
                time.sleep(20)



    
    def add_alert(self):
        currTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        newAlert = Alerts.objects.create(alertType=TAMPERING, clip=TAMPERING_MEDIA + self.filename + ".jpg", time=currTime, checked=False, cid=self.camobj)







