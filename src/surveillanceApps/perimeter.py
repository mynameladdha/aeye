import cv2
import numpy as np
from .models import Camera, Alerts, Application
import threading
from . import YOLO_tiny_tf
from .utils import non_max_suppression_fast, is_now_in_time_period
from .foreground_background import contains_human_contour
from uuid import uuid4
import datetime
import json
#from skimage.measure import compare_ssim
import imutils
import time
from .camera import VideoCamera
from abc import ABC, abstractmethod
from .allApps import App,thread_dic#,yolo
#from .yolov2.test_yolo import _main 
from .constants import USERNAME,PASSWORD,ACTION_TO_ALERT,PERIMETER,INTRUSION,TAMPERING,OVERCROWDING,STARTX_REC,STARTY_REC,WIDTH_REC,HEIGHT_REC,CAMERA_NAME,CAMERA_URL,CLIP_PATH,IMAGE_URL,PERIMETER_MEDIA
from .tfod.app import _main1

class PerimeterMonitoring(App):

    def __init__(self, camurl, camuname, campassword, req, startx, starty, width, height, sttime="", endtime=""):
        self.camurl = camurl
        self.camuname = camuname
        self.campassword = campassword
        self.req = req
        self.sttime = sttime
        self.endtime = endtime
        self.cam = None
        self.camobj = None
        self.startx = startx
        self.starty = starty
        self.width = width
        self.height = height



    def initialize_camera(self):
        self.cam = VideoCamera(self.camurl, self.camuname, self.campassword)

    def add_application(self):
        self.camobj = Camera.objects.get(url=self.camurl)
        Application.objects.create(cid=self.camobj, appName=PERIMETER, uid=self.req.user)

    def perimeter_monitoring(self):
        print("started")
        l=threading.current_thread().getName().split(" ")
        checkToStoptime=l[1]
        checkToStopurl=l[0]
        self.initialize_camera()
        self.add_application()
        while True:
            if self.endtime!="" and self.endtime<datetime.datetime.now().strftime("%H:%M:%S"):
                continue
            elif self.sttime!="" and self.sttime>datetime.datetime.now().strftime("%H:%M:%S"):
                continue
            elif self.startx=="" or self.starty=="":
                continue        
            if checkToStopurl in thread_dic and checkToStoptime<thread_dic[checkToStopurl]:
                print("stopping previous thread")
                break
            img = self.cam.get_frame()
            
            if img is None:
                continue
            img=cv2.resize(img, (400, 400)) 
            
            self.startx=int(float(self.startx))
            self.starty=int(float(self.starty))
            self.width=int(float(self.width))
            self.height=int(float(self.height))
            lasty=self.starty+self.height
            lastx=self.startx+self.width
            img=img[self.starty:lasty,self.startx:lastx]
            self.filename="cropped"
            cv2.imwrite(PERIMETER_MEDIA + self.filename + ".jpg", img)
            img=_main1(img)
            if img is None:
                continue
            self.filename = uuid4().hex
            cv2.imwrite(PERIMETER_MEDIA + self.filename + ".jpg", img)
            print("perimeter ended")
            self.add_alert()
                   
            
                
    
    def add_alert(self):
        currTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        newAlert = Alerts.objects.create(alertType=PERIMETER, clip=PERIMETER_MEDIA + self.filename + ".jpg", time=currTime, checked=False, cid=self.camobj)

