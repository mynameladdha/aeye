from .models import object_detection
from .config import config
import cv2
import tensorflow as tf
import time
import os

Base=os.path.dirname(os.path.abspath(__file__))

model_name = config.models["1"]
net = object_detection.Net(graph_fp=Base+'/mobilenet/frozen_inference_graph.pb',
                           labels_fp=Base+'/data/label.pbtxt',
                           num_classes=90,
                           threshold=0.25)

IMAGE_SIZE = 320


def _main1(frame):
    s=time.time()
    resize_frame = cv2.resize(frame, (IMAGE_SIZE, IMAGE_SIZE))
    img=net.predict(img=resize_frame, display_img=frame)
    print(time.time()-s)
    return img

if __name__ == '__main__':
    pass
