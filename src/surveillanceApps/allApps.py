from . import YOLO_tiny_tf
from abc import ABC, abstractmethod
import copy
import queue
import os
import numpy as np
#from keras.models import load_model

class App(ABC):
    
    @abstractmethod
    def initialize_camera(self):
        pass

    @abstractmethod
    def add_application(self):
        pass

    @abstractmethod
    def add_alert(self):
        pass

thread_dic={}
#yolo = YOLO_tiny_tf.YOLO_TF()



