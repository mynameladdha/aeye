import cv2
import requests
import numpy as np
import datetime


class VideoCamera(object):

    def __init__(self, url, admin='', password=''):
        self.url = url
        self.admin = admin
        self.password = password

    def get_frame(self):
        print(datetime.datetime.now())

        r = requests.get(self.url, auth=(self.admin, self.password), stream=True)
        
        if(r.status_code == 200):
            byte = bytes()
            for chunk in r.iter_content(chunk_size=1024):

                byte += chunk
                a = byte.find(b'\xff\xd8')
                b = byte.find(b'\xff\xd9')
                if a != -1 and b != -1:
                    jpg = byte[a:b + 2]
                    byte = byte[b + 2:]
                    i = cv2.imdecode(np.fromstring(
                        jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    print(datetime.datetime.now())
                    return i

        else:
            return None
