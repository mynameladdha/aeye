import cv2
import numpy as np
from .models import Camera, Alerts, Application
import threading
from .utils import non_max_suppression_fast, is_now_in_time_period
from .foreground_background import contains_human_contour
from uuid import uuid4
import datetime
import json
#from skimage.measure import compare_ssim
import imutils
import time
from .camera import VideoCamera
from abc import ABC, abstractmethod
from .allApps import App,thread_dic#,yolo
from .constants import USERNAME,PASSWORD,ACTION_TO_ALERT,PERIMETER,INTRUSION,TAMPERING,OVERCROWDING,STARTX_REC,STARTY_REC,WIDTH_REC,HEIGHT_REC,CAMERA_NAME,CAMERA_URL,CLIP_PATH,IMAGE_URL,INTRUSION_MEDIA
import copy
#from .yolov2.test_yolo import _main 
from .tfod.app import _main1


class IntrusionDetection(App):


    def __init__(self,camurl, camuname, campassword, req, sttime="", endtime=""):
        self.camurl=camurl
        self.camuname=camuname
        self.campassword=campassword
        self.req=req
        self.sttime=sttime
        self.endtime=endtime
        self.cam=None
        self.camobj=Camera.objects.get(url=self.camurl)
        self.filename=None
        

        



    def initialize_camera(self):
        self.cam = VideoCamera(self.camurl, self.camuname, self.campassword)
        

    def add_application(self):
        Application.objects.create(cid=self.camobj, appName=INTRUSION, uid=self.req.user)

    def intrusion_detection(self):
        
        print("started")
        l=threading.current_thread().getName().split(" ")
        checkToStoptime=l[1]
        checkToStopurl=l[0]
        self.initialize_camera()
        self.add_application()
        while True:
            if self.endtime!="" and self.endtime<datetime.datetime.now().strftime("%H:%M:%S"):
                continue
            elif self.sttime!="" and self.sttime>datetime.datetime.now().strftime("%H:%M:%S"):
                continue    
            if checkToStopurl in thread_dic and checkToStoptime<thread_dic[checkToStopurl]:
                print("stopping previous thread")
                break
            img = self.cam.get_frame()
            if img is None:
                continue
            img=_main1(img)
            print("tusshar")    
            print(img)
            if img is None:
                continue
            self.filename = uuid4().hex
            cv2.imwrite(INTRUSION_MEDIA + self.filename + ".jpg", img)
            print("intrusion completed")
            self.add_alert()
            
                
    
    def add_alert(self):
        currTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        newAlert = Alerts.objects.create(alertType=INTRUSION, clip=INTRUSION_MEDIA+self.filename + ".jpg", time=currTime, checked=False, cid=self.camobj)


