from django.db import models


# Create your models here.

APPROVAL_CHOICES = (
    ('intrusion', 'Intursion'),
    ('tampering', 'Tampering'),
    ('perimeter', 'Perimeter'),
    ('overcrowding', 'Overcrowding'),
    
)

class Camera(models.Model):
    
    camName = models.CharField(max_length=15,unique=True)
    url = models.CharField(max_length=150,unique=True)
    uid = models.ForeignKey('auth.User')
    username=models.CharField(max_length=50,blank=True,null=True)
    password=models.CharField(max_length=50,blank=True,null=True)

    def __str__(self):
        return self.camName


class Application(models.Model):

    cid= models.ForeignKey('Camera')
    appName=models.CharField(max_length=20,choices=APPROVAL_CHOICES)
    st= models.DateTimeField(blank=True,null=True)
    end=models.DateTimeField(blank=True,null=True)
    uid = models.ForeignKey('auth.User')
    
    def __str__(self):
        return self.appName

class Alerts(models.Model):
    
    alertType= models.CharField(max_length=20,choices=APPROVAL_CHOICES)
    clip =models.CharField(max_length=40)
    time=models.CharField(max_length=20)
    cid= models.ForeignKey('Camera')
    checked=models.BooleanField()
    relevance=models.CharField(max_length=20,default="not yet updated")
    responseTime=models.CharField(max_length=10,default="not yet updated")

    def __str__(self):
        return self.alertType
    
