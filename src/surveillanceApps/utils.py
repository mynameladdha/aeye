import numpy as np
import cv2 
import os
import glob
# import dlib
from . import YOLO_tiny_tf
import imutils
import math

def is_now_in_time_period(startTime, endTime, nowTime):
    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else: #Over midnight
        return nowTime >= startTime or nowTime <= endTime

def most_common(lst):
    return max(set(lst), key=lst.count)

def item_frequency(lst,item):
    freqDict = {x:lst.count(x) for x in set(lst)}
    if item in freqDict.keys():
        return freqDict[item]
    else:
        return None

def match_images(query_image, person_image, match_thresh):
    # Initiate SIFT detector
    # Initiate SIFT detector
    orb = cv2.ORB_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = orb.detectAndCompute(query_image,None)
    kp2, des2 = orb.detectAndCompute(person_image,None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    # Match descriptors.
    matches = bf.match(des1,des2)
    # Sort them in the order of their distance.

    if len(matches)>0:
        print(str(len(matches))+" matching points found")
        matches = sorted(matches, key = lambda x:x.distance)
        sum_of_top_distances = sum([x.distance for x in matches[:10]])
        print("sum_of_top_distances is equal",sum_of_top_distances)
        if sum_of_top_distances<match_thresh:
            return True
        else:
            return False
    else:
        print("Zero matching points found")
        return False

def distance_rects(rect1,rect2):
    [x1,y1,x2,y2] = rect1; [x3,y3,x4,y4] = rect2
    p1 = [(x1+x2)/2,(y1+y2)/2]; p2 = [(x3+x4)/2,(y3+y4)/2]
    return math.hypot(p1[0]-p2[0],p1[1]-p2[1])

def closest_rectangle(query_rectangle,existing_rectangles):
    distances = [distance_rects(x,query_rectangle) for x in existing_rectangles]
    return np.argmin(distances)

def min_intersect_fg_bbox(fgmask,rectangle,gaussian = (5,5),gray_thresh = 45):
    [x1,y1,x2,y2] = rectangle
    cropped = fgmask[y1:y2,x1:x2]
    gray = cv2.GaussianBlur(cropped, gaussian, 0)
    thresh = cv2.threshold(gray,gray_thresh , 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=2)
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
    cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    if len(cnts)>0:
        c = max(cnts, key=cv2.contourArea)
        extLeft = tuple(c[c[:, :, 0].argmin()][0])
        extRight = tuple(c[c[:, :, 0].argmax()][0])
        extTop = tuple(c[c[:, :, 1].argmin()][0])
        extBot = tuple(c[c[:, :, 1].argmax()][0])
        return np.array([x1+extLeft[0],y1+extTop[1],x1+extRight[0],y1+extBot[1]])
    else:
        return rectangle

def bbox_of_contour(c):
    extLeft = tuple(c[c[:, :, 0].argmin()][0])
    extRight = tuple(c[c[:, :, 0].argmax()][0])
    extTop = tuple(c[c[:, :, 1].argmin()][0])
    extBot = tuple(c[c[:, :, 1].argmax()][0])
    return np.array([extLeft[0],extTop[1],extRight[0],extBot[1]])

def fraction_of_overlap(bbox1,bbox2):
    [XA1,YA1,XA2,YA2] = bbox1
    [XB1,YB1,XB2,YB2] = bbox2
    SA = (XA2-XA1+1)*(YA2-YA1+1)
    SB = (XB2-XB1+1)*(YB2-YB1+1)
    SI = max(0, max(XA2, XB2) - min(XA1, XB1)) * max(0, max(YA2, YB2) - min(YA1, YB1))
    SU = SA + SB - SI
    return SI/SU

def tracker_pos_to_points(tracker_obj):
    coord_left = tracker_obj.get_position().left()
    coord_right = tracker_obj.get_position().right()
    coord_top = tracker_obj.get_position().top()
    coord_bottom = tracker_obj.get_position().bottom()
    return [coord_left,coord_top,coord_right,coord_bottom]

def is_out_of_bounds(rect,img,thresh):
    # or abs(rect[2]-img.shape[0])<thresh
    if abs(rect[0])<thresh:
        return True
    else:
        return False

def non_max_suppression_fast(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes 
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > overlapThresh)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int")
