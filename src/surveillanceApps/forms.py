from django import forms

class CameraForm(forms.Form):
    name=forms.CharField(max_length=15,widget=forms.TextInput(attrs={'class':"form-control"}))
    url=forms.URLField(max_length=150,widget=forms.TextInput(attrs={'class':"form-control"}))
    username=forms.CharField(max_length=30,widget=forms.TextInput(attrs={'class':"form-control"}),required=False)
    password=forms.CharField(max_length=30,widget=forms.PasswordInput(attrs={'class':"form-control"}),required=False)
   
